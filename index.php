<?php 

require_once("Core/Controller.php");
require_once("Core/DataBase.php");
require_once("Core/Loader.php");
require_once("core/Views.php");

$index_controller = "Users";

if ($_GET && isset($_GET["controller"])) {
    $index_controller = $_GET["controller"];
    require_once(__DIR__ . "/Controllers/". $_GET["controller"] . ".php");
}
else{
    require_once(__DIR__ . "/Controllers/" . $index_controller . ".php");
}
$obj = new $index_controller();