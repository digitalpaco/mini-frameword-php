<?php


class UserModel extends DataBase
{
    public function __construct()
    {
        parent::__construct();
    }
    public function getUsers(){
        $result = $this->mysql->query("SELECT * FROM users");
        return $result->fetchAll();
    }
}