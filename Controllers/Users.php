<?php



class Users extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }
    public function index (){
        $loader = new Loader("UserModel");
        $UserModel = new UserModel();
        $users = $UserModel->getUsers();
        $userView = new Views("users/users.php", compact("users"));
    }
}