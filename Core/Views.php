<?php



class Views
{
    public function __construct($view, $data = null)
    {
        if (file_exists("./Views/$view")) {
            require_once("./Views/$view");
        }
        else{
            die("Sitio no encontrado.");
        }
    }
}