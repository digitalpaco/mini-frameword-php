<?php


class DataBase
{
    public $mysql = null;
    public function __construct()
    {
        try{
            $this->mysql = $this->getConnection();
        }catch(PDOException $ex){
            die('mensaje de error: ' . $ex->getMessage());
        }
    }
    private function getConnection() {
        require_once("./Config/env.php");
        $dns = 'mysql:host=' . HOST_DB . ';dbname=' . NAME_DB . ';charset=' . CHARSET;
        $pdo = new PDO($dns, USER_DB, PASSWORD_DB, OPT_PDO);
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $pdo;
    }
}
